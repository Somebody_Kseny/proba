#include <iostream>
#include <SFML/Graphics.hpp>
#include "cmake_modules/game.h"

int main() {
    std::cout << __cplusplus << std::endl;
    Game game;
    game.run();
}