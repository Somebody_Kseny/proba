//
// Created by kseny on 17.11.2019.
//

#include <iostream>
#include "game.h"
#include "tinyxml2.h"
#include "TmxLevel.h"

Game::Game()
        : mWindow(sf::VideoMode(1920,1080), "SFML Application"), mPlayer(){ //можно сделать не цифры, а получать разрешение экрана

    if (!mTexture.loadFromFile("source/moon.png"))
    {
// Handle loading error
    }
    if (!backTexture.loadFromFile("source/stars.png"))
    {
// Handle loading error
    }
    if (!forFlower.loadFromFile("source/saturn.png"))
    {
// Handle loading error
    }
    mBack.setTexture(backTexture);
    mBack.setPosition(0.f, 0.f);
    mBack.setOrigin(10.f, 0.f);
    std::cout << "im here... 0" << std::endl;
    mPlayer.setTexture(mTexture);
    mPlayer.setPosition(500.f, 300.f);
    mPlayer.setOrigin(10.f, 20.f);
}

void Game::run()  {
    std::cout << "im here... 1" << std::endl;
    sf::Clock clock;
    sf::Time timeSinceLastUpdate = sf::Time::Zero;

    try{
        mLevel.LoadFromFile("source/show.tmx"); //HERE
        std::cout << "im here... 2" << std::endl;
    }
    catch (std::runtime_error &ex)
    {
        std::cout << "im here... 22" << std::endl;
        std::cerr << ex.what() << std::endl;
    }

    flower = mLevel.GetFirstObject("rect");
    std::cout << flower.name << std::endl;
    mFlower.setTexture(forFlower);
    mFlower.setPosition(flower.rect.left, (flower.rect.top - flower.rect.height));
    mFlower.setOrigin(0.f, 0.f);// flower.rect.width);

    while (mWindow.isOpen())
    {
        processEvents();
        timeSinceLastUpdate += clock.restart();
        while (timeSinceLastUpdate > TimePerFrame){
            timeSinceLastUpdate -= TimePerFrame;
            processEvents();
            update(TimePerFrame);
        }
        render();
    }
}

void Game::processEvents()  {
    sf::Event event;
    while (mWindow.pollEvent(event))
    {
        switch (event.type)
        {
            case sf::Event::KeyPressed:
                handlePlayerInput(event.key.code, true);
                break;
            case sf::Event::KeyReleased:
                handlePlayerInput(event.key.code, false);
                break;
            case sf::Event::Closed:
                mWindow.close();
                break;
        }
    }
}

void Game::handlePlayerInput(sf::Keyboard::Key key, bool isPressed) {
    if (key == sf::Keyboard::W)
        mIsMovingUp = isPressed;
    else if (key == sf::Keyboard::S)
        mIsMovingDown = isPressed;
    else if (key == sf::Keyboard::A)
        mIsMovingLeft = isPressed;
    else if (key == sf::Keyboard::D)
        mIsMovingRight = isPressed;
    else if (key == sf::Keyboard::X)
        mWindow.close();
}

void Game::update(sf::Time deltaTime) {
    sf::Vector2f movement(0.f, 0.f);
    if (mIsMovingUp==true) {
        movement.y -= speedOfHero;
        std::cout << "up" << std::endl;
    }
    if (mIsMovingDown){
        movement.y += speedOfHero;
        std::cout << "down" << std::endl;
    }
    if (mIsMovingLeft){
        movement.x -= speedOfHero;
        std::cout << "left" << std::endl;
    }
    if (mIsMovingRight){
        movement.x += speedOfHero;
        std::cout << "right" << std::endl;
    }
    mPlayer.move(movement * deltaTime.asSeconds());
}

void Game::render()  {
    mWindow.clear();
    mWindow.draw(mBack);
    mLevel.Draw(mWindow);
    mWindow.draw(mFlower);
    mWindow.draw(mPlayer);
    mWindow.display();
}
