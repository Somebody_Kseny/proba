//
// Created by kseny on 17.11.2019.
//

#ifndef GAME_DEV_GAME_H
#define GAME_DEV_GAME_H


#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Font.hpp>
#include "TmxLevel.h"


class Game {
    public:
        Game();
        void run();
    private:
        void processEvents();
        void update(sf::Time);
        void render();
        void handlePlayerInput(sf::Keyboard::Key, bool);
        bool mIsMovingDown = false, mIsMovingUp = false, mIsMovingLeft = false, mIsMovingRight = false;
        const sf::Time TimePerFrame = sf::seconds(1.f / 60.f);
        const int speedOfHero = 180.f;
    private:
        sf::RenderWindow mWindow;
        sf::Texture mTexture;
        sf::Sprite mPlayer;
        sf::Texture backTexture;
        sf::Sprite mBack;
        TmxLevel mLevel;
        TmxObject flower;
        //TmxObject rect;
        sf::Texture forFlower;
        sf::Sprite mFlower;
};

#endif //GAME_DEV_GAME_H
